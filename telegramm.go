package main

import (
	"bytes"
	"io"
	"log"
	"os/exec"
	"strings"

	"github.com/Syfaro/telegram-bot-api"
	"github.com/kr/pty"
)

func main() {
	telegramBotToken := ""
	// используя токен создаем новый инстанс бота
	bot, err := tgbotapi.NewBotAPI(telegramBotToken)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// u - структура с конфигом для получения апдейтов
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	// используя конфиг u создаем канал в который будут прилетать новые сообщения
	updates, err := bot.GetUpdatesChan(u)

	// в канал updates прилетают структуры типа Update
	// вычитываем их и обрабатываем
	for update := range updates {
		// универсальный ответ на любое сообщение
		reply := "Введи /help для справки"
		if update.Message == nil {
			continue
		}

		// логируем от кого какое сообщение пришло
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		splitCommand := strings.Split(update.Message.Text, " ")
		c := exec.Command(splitCommand[0])
		log.Printf("[%s] run command: %v", update.Message.From.UserName, splitCommand)
		if len(splitCommand) > 1 {
			c = exec.Command(splitCommand[0], splitCommand[1:]...)
		}
		f, err := pty.Start(c)
		if err != nil {
			panic(err)
		}

		var buf bytes.Buffer
		io.Copy(&buf, f)
		log.Printf("[%s] command out: %s", update.Message.From.UserName, reply)

		reply = buf.String()
		// создаем ответное сообщение
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
		// отправляем
		bot.Send(msg)
	}
}
